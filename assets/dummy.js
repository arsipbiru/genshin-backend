module.exports = {
    "format": "GOOD",
    "version": 1,
    "source": "Inventory_Kamera",
    "characters": [{
            "key": "HuTao",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 10,
                "skill": 9,
                "burst": 9
            }
        },
        {
            "key": "Tartaglia",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 9,
                "skill": 9,
                "burst": 9
            }
        },
        {
            "key": "Zhongli",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 8,
                "skill": 9,
                "burst": 9
            }
        },
        {
            "key": "Xingqiu",
            "level": 70,
            "constellation": 3,
            "ascension": 4,
            "talent": {
                "auto": 3,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Albedo",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 7,
                "skill": 8,
                "burst": 8
            }
        },
        {
            "key": "Xiao",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 9,
                "skill": 8,
                "burst": 9
            }
        },
        {
            "key": "Venti",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 8,
                "skill": 8,
                "burst": 8
            }
        },
        {
            "key": "KamisatoAyaka",
            "level": 90,
            "constellation": 0,
            "ascension": 6,
            "talent": {
                "auto": 8,
                "skill": 8,
                "burst": 8
            }
        },
        {
            "key": "Diluc",
            "level": 90,
            "constellation": 1,
            "ascension": 6,
            "talent": {
                "auto": 8,
                "skill": 9,
                "burst": 9
            }
        },
        {
            "key": "Qiqi",
            "level": 80,
            "constellation": 0,
            "ascension": 5,
            "talent": {
                "auto": 5,
                "skill": 5,
                "burst": 6
            }
        },
        {
            "key": "Jean",
            "level": 70,
            "constellation": 1,
            "ascension": 4,
            "talent": {
                "auto": 3,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Keqing",
            "level": 40,
            "constellation": 0,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Noelle",
            "level": 80,
            "constellation": 6,
            "ascension": 5,
            "talent": {
                "auto": 6,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Ningguang",
            "level": 80,
            "constellation": 6,
            "ascension": 5,
            "talent": {
                "auto": 6,
                "skill": 5,
                "burst": 4
            }
        },
        {
            "key": "Fischl",
            "level": 80,
            "constellation": 4,
            "ascension": 5,
            "talent": {
                "auto": 6,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Barbara",
            "level": 80,
            "constellation": 5,
            "ascension": 5,
            "talent": {
                "auto": 2,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Yanfei",
            "level": 80,
            "constellation": 2,
            "ascension": 5,
            "talent": {
                "auto": 6,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Bennett",
            "level": 80,
            "constellation": 0,
            "ascension": 5,
            "talent": {
                "auto": 6,
                "skill": 6,
                "burst": 6
            }
        },
        {
            "key": "Sucrose",
            "level": 70,
            "constellation": 2,
            "ascension": 4,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Rosaria",
            "level": 70,
            "constellation": 1,
            "ascension": 5,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Diona",
            "level": 70,
            "constellation": 5,
            "ascension": 5,
            "talent": {
                "auto": 2,
                "skill": 6,
                "burst": 2
            }
        },
        {
            "key": "Chongyun",
            "level": 70,
            "constellation": 6,
            "ascension": 4,
            "talent": {
                "auto": 4,
                "skill": 6,
                "burst": 3
            }
        },
        {
            "key": "Kaeya",
            "level": 70,
            "constellation": 1,
            "ascension": 4,
            "talent": {
                "auto": 1,
                "skill": 4,
                "burst": 4
            }
        },
        {
            "key": "Razor",
            "level": 70,
            "constellation": 2,
            "ascension": 5,
            "talent": {
                "auto": 2,
                "skill": 2,
                "burst": 2
            }
        },
        {
            "key": "Lisa",
            "level": 60,
            "constellation": 0,
            "ascension": 3,
            "talent": {
                "auto": 2,
                "skill": 2,
                "burst": 2
            }
        },
        {
            "key": "Xiangling",
            "level": 52,
            "constellation": 4,
            "ascension": 3,
            "talent": {
                "auto": 2,
                "skill": 2,
                "burst": 2
            }
        },
        {
            "key": "KujouSara",
            "level": 50,
            "constellation": 1,
            "ascension": 3,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Amber",
            "level": 50,
            "constellation": 0,
            "ascension": 3,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Aloy",
            "level": 40,
            "constellation": 0,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Xinyan",
            "level": 40,
            "constellation": 1,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Sayu",
            "level": 20,
            "constellation": 2,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Beidou",
            "level": 20,
            "constellation": 4,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        },
        {
            "key": "Thoma",
            "level": 20,
            "constellation": 0,
            "ascension": 1,
            "talent": {
                "auto": 1,
                "skill": 1,
                "burst": 1
            }
        }
    ],
    "artifacts": [{
            "setKey": "PaleFlame",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Zhongli",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 65
                },
                {
                    "key": "def",
                    "value": 35
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp",
                    "value": 777
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "KamisatoAyaka",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 6.2
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critDMG_",
                    "value": 24.1
                },
                {
                    "key": "atk",
                    "value": 18
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Zhongli",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 58
                },
                {
                    "key": "atk_",
                    "value": 15.7
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Zhongli",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 448
                },
                {
                    "key": "hp_",
                    "value": 14
                },
                {
                    "key": "def_",
                    "value": 13.1
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Zhongli",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 49
                },
                {
                    "key": "enerRech_",
                    "value": 11.7
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 478
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Diona",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 11.7
                },
                {
                    "key": "atk",
                    "value": 49
                },
                {
                    "key": "critDMG_",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 7.4
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Zhongli",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 16.2
                },
                {
                    "key": "critDMG_",
                    "value": 17.9
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Tartaglia",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "atk_",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 12.3
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Tartaglia",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 448
                },
                {
                    "key": "atk_",
                    "value": 19.2
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Tartaglia",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 70
                },
                {
                    "key": "hp_",
                    "value": 9.9
                },
                {
                    "key": "atk",
                    "value": 29
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Tartaglia",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 14.6
                },
                {
                    "key": "enerRech_",
                    "value": 16.2
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "Noelle",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 9.7
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 20.2
                },
                {
                    "key": "critRate_",
                    "value": 10.5
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "Noelle",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 9.9
                },
                {
                    "key": "critDMG_",
                    "value": 18.7
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk",
                    "value": 37
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Albedo",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 478
                },
                {
                    "key": "atk",
                    "value": 49
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 12.4
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Fischl",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 6.2
                },
                {
                    "key": "def_",
                    "value": 10.9
                },
                {
                    "key": "eleMas",
                    "value": 37
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Fischl",
            "lock": true,
            "substats": [{
                    "key": "eleMas",
                    "value": 37
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "def_",
                    "value": 21.9
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "enerRech_",
                    "value": 17.5
                },
                {
                    "key": "hp_",
                    "value": 9.9
                },
                {
                    "key": "atk",
                    "value": 31
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "hp_",
                    "value": 8.7
                },
                {
                    "key": "critRate_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 17.5
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "HuTao",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 12.4
                },
                {
                    "key": "eleMas",
                    "value": 42
                },
                {
                    "key": "critDMG_",
                    "value": 13.2
                },
                {
                    "key": "critRate_",
                    "value": 10.1
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "enerRech_",
                    "value": 9.7
                },
                {
                    "key": "critRate_",
                    "value": 6.6
                },
                {
                    "key": "atk",
                    "value": 49
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Yanfei",
            "lock": true,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk_",
                    "value": 15.2
                },
                {
                    "key": "atk",
                    "value": 54
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Yanfei",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 11.7
                },
                {
                    "key": "hp_",
                    "value": 9.3
                },
                {
                    "key": "atk_",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "HuTao",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 11.3
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "def",
                    "value": 39
                },
                {
                    "key": "atk_",
                    "value": 9.9
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "HuTao",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 9.7
                },
                {
                    "key": "atk",
                    "value": 31
                },
                {
                    "key": "hp",
                    "value": 478
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Diluc",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 9.7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "eleMas",
                    "value": 44
                },
                {
                    "key": "hp",
                    "value": 747
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 47
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Tartaglia",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critRate_",
                    "value": 12.8
                },
                {
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "eleMas",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Diluc",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 40
                },
                {
                    "key": "critDMG_",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 9.3
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "Yanfei",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "def_",
                    "value": 21.1
                },
                {
                    "key": "def",
                    "value": 42
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "HuTao",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 10.1
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 807
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 16.8
                },
                {
                    "key": "hp_",
                    "value": 8.7
                },
                {
                    "key": "hp",
                    "value": 538
                },
                {
                    "key": "def_",
                    "value": 5.1
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 11.7
                },
                {
                    "key": "hp",
                    "value": 747
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 15.7
                },
                {
                    "key": "def",
                    "value": 67
                },
                {
                    "key": "critRate_",
                    "value": 7
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 13.2
                },
                {
                    "key": "hp",
                    "value": 448
                },
                {
                    "key": "hp_",
                    "value": 10.5
                },
                {
                    "key": "def_",
                    "value": 12.4
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "HuTao",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 10.5
                },
                {
                    "key": "eleMas",
                    "value": 42
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def_",
                    "value": 14.6
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 58
                },
                {
                    "key": "atk_",
                    "value": 8.7
                },
                {
                    "key": "def",
                    "value": 46
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Jean",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk_",
                    "value": 9.9
                },
                {
                    "key": "eleMas",
                    "value": 79
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Venti",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "eleMas",
                    "value": 79
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Jean",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 56
                },
                {
                    "key": "eleMas",
                    "value": 40
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 12.4
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Xiao",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 11
                },
                {
                    "key": "def",
                    "value": 32
                },
                {
                    "key": "eleMas",
                    "value": 44
                },
                {
                    "key": "atk_",
                    "value": 12.2
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Diluc",
            "lock": true,
            "substats": [{
                    "key": "atk_",
                    "value": 11.1
                },
                {
                    "key": "hp_",
                    "value": 9.3
                },
                {
                    "key": "def",
                    "value": 46
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Albedo",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 14.4
                },
                {
                    "key": "def",
                    "value": 37
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Diluc",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "eleMas",
                    "value": 75
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 478
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Diona",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 10.1
                },
                {
                    "key": "atk_",
                    "value": 11.1
                },
                {
                    "key": "def",
                    "value": 39
                },
                {
                    "key": "hp",
                    "value": 568
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Fischl",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 747
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def_",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Diluc",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 747
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 19.8
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 14.2
                },
                {
                    "key": "atk_",
                    "value": 8.2
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 12.4
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "Noelle",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 56
                },
                {
                    "key": "hp",
                    "value": 538
                },
                {
                    "key": "atk_",
                    "value": 9.3
                },
                {
                    "key": "eleMas",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "Fischl",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 598
                },
                {
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "critDMG_",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "KamisatoAyaka",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "critDMG_",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 8.7
                },
                {
                    "key": "eleMas",
                    "value": 40
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "KamisatoAyaka",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 9.3
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 24.6
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "KamisatoAyaka",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 49
                },
                {
                    "key": "critDMG_",
                    "value": 18.7
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "enerRech_",
                    "value": 11
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "hp_",
                    "value": 16.3
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 20,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "KamisatoAyaka",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def_",
                    "value": 23.3
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "flower",
            "level": 18,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Noelle",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 11.7
                },
                {
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "critRate_",
                    "value": 9.3
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 17,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Bennett",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "atk",
                    "value": 37
                },
                {
                    "key": "atk_",
                    "value": 14.6
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "plume",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Noelle",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 747
                },
                {
                    "key": "eleMas",
                    "value": 37
                },
                {
                    "key": "atk_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "circlet",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Albedo",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 49
                },
                {
                    "key": "enerRech_",
                    "value": 11.7
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Xingqiu",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 17.5
                },
                {
                    "key": "atk_",
                    "value": 11.1
                },
                {
                    "key": "atk",
                    "value": 33
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "goblet",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Venti",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 5.4
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "eleMas",
                    "value": 77
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Yanfei",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 63
                },
                {
                    "key": "critRate_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 69
                },
                {
                    "key": "atk",
                    "value": 31
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 13.2
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 9.9
                },
                {
                    "key": "enerRech_",
                    "value": 11
                },
                {
                    "key": "atk",
                    "value": 29
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Xiao",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 20.2
                },
                {
                    "key": "atk",
                    "value": 27
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Xiao",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "critDMG_",
                    "value": 12.4
                },
                {
                    "key": "critRate_",
                    "value": 7.4
                },
                {
                    "key": "atk",
                    "value": 33
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Xiao",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "enerRech_",
                    "value": 11
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp",
                    "value": 747
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 16,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "Albedo",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 9.3
                },
                {
                    "key": "def",
                    "value": 63
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "eleMas",
                    "value": 42
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 13,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Bennett",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 508
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 13
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "goblet",
            "level": 13,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Bennett",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp",
                    "value": 478
                },
                {
                    "key": "eleMas",
                    "value": 42
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Kaeya",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 448
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 11
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def",
                    "value": 37
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 37
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Venti",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 12.4
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk",
                    "value": 33
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "Venti",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 35
                },
                {
                    "key": "hp",
                    "value": 568
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "goblet",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "anemo_dmg_",
            "location": "Xiao",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def",
                    "value": 37
                },
                {
                    "key": "enerRech_",
                    "value": 17.5
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 12,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "Bennett",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 11.7
                },
                {
                    "key": "eleMas",
                    "value": 35
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 8,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Barbara",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 9.3
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 8,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Jean",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "critDMG_",
                    "value": 11.7
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "plume",
            "level": 8,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Barbara",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 42
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp",
                    "value": 239
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 8,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Barbara",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 11
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "def",
                    "value": 35
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 6,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Xingqiu",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 5,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Xiangling",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.1
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Xiangling",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp",
                    "value": 538
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk",
                    "value": 33
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "hp",
                    "value": 269
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 4,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Venti",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "OceanHuedClam",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HuskOfOpulentDreams",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HuskOfOpulentDreams",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "cryo_dmg_",
            "location": "Chongyun",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ShimenawasReminiscence",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "PaleFlame",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Diona",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Diona",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "Diona",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TenacityOfTheMillelith",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.1
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "def_",
                    "value": 5.1
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "anemo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "def",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "HeartOfDepth",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Keqing",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "RetracingBolide",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 6.6
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Albedo",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "cryo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ArchaicPetra",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BloodstainedChivalry",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Chongyun",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Bennett",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Chongyun",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Xiangling",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Xingqiu",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "Chongyun",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "Xingqiu",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "atk",
                    "value": 14
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Xiangling",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "Chongyun",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "NoblesseOblige",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Xingqiu",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "cryo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "CrimsonWitchOfFlames",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def_",
                    "value": 7.3
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 239
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "atk_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "hp_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 269
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "hp",
                    "value": 239
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Jean",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Jean",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "physical_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "ViridescentVenerer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 18
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 5.1
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Keqing",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "eleMas",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "def",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 5.1
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "Keqing",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "hp_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "atk_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Keqing",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "def_",
                    "value": 7.3
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "Keqing",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp",
                    "value": 209
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 239
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "cryo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "atk",
                    "value": 14
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "physical_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp",
                    "value": 209
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "physical_dmg_",
            "location": "Fischl",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "anemo_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "electro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "eleMas",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 14
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp",
                    "value": 299
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "hp_",
                    "value": 5.3
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp_",
                    "value": 4.7
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "critDMG_",
                    "value": 7
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "critDMG_",
                    "value": 7
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Barbara",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "critDMG_",
                    "value": 7
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Barbara",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "MaidenBeloved",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def_",
                    "value": 6.6
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "def_",
                    "value": 6.6
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 21
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "eleMas",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critRate_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.5
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "def",
                    "value": 16
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "Kaeya",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "flower",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 19
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "eleMas",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "plume",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp_",
                    "value": 5.3
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "Kaeya",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                },
                {
                    "key": "hp",
                    "value": 239
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.5
                },
                {
                    "key": "def",
                    "value": 23
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "critRate_",
                    "value": 3.1
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 7
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 269
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "eleMas",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 5.8
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "sands",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "enerRech_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "def_",
                    "value": 7.3
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "physical_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 3.1
                },
                {
                    "key": "def_",
                    "value": 5.1
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 4.5
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "pyro_dmg_",
            "location": "Xiangling",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 5.8
                },
                {
                    "key": "critDMG_",
                    "value": 5.4
                },
                {
                    "key": "def",
                    "value": 21
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "physical_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "eleMas",
                    "value": 23
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "def_",
                    "value": 5.8
                },
                {
                    "key": "eleMas",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "geo_dmg_",
            "location": "",
            "lock": true,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "eleMas",
                    "value": 21
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "cryo_dmg_",
            "location": "Kaeya",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "enerRech_",
                    "value": 5.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 18
                },
                {
                    "key": "critDMG_",
                    "value": 6.2
                },
                {
                    "key": "atk_",
                    "value": 5.3
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "hydro_dmg_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.1
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "def",
                    "value": 16
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "eleMas",
                    "value": 23
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "critRate_",
                    "value": 3.9
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 6.5
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 16
                },
                {
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 7.8
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "heal_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.1
                },
                {
                    "key": "atk",
                    "value": 19
                },
                {
                    "key": "hp",
                    "value": 209
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "BlizzardStrayer",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 5,
            "mainStatKey": "critDMG_",
            "location": "Kaeya",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "hp",
                    "value": 299
                },
                {
                    "key": "critRate_",
                    "value": 2.7
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "sands",
            "level": 16,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "Yanfei",
            "lock": false,
            "substats": [{
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "enerRech_",
                    "value": 4.1
                },
                {
                    "key": "def",
                    "value": 50
                },
                {
                    "key": "hp",
                    "value": 167
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "goblet",
            "level": 16,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 15
                },
                {
                    "key": "enerRech_",
                    "value": 12.4
                },
                {
                    "key": "hp",
                    "value": 215
                },
                {
                    "key": "eleMas",
                    "value": 13
                }
            ]
        },
        {
            "setKey": "Lavawalker",
            "slotKey": "goblet",
            "level": 16,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 13
                },
                {
                    "key": "atk",
                    "value": 11
                },
                {
                    "key": "enerRech_",
                    "value": 10.4
                },
                {
                    "key": "eleMas",
                    "value": 35
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "circlet",
            "level": 16,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 3.3
                },
                {
                    "key": "critRate_",
                    "value": 2.5
                },
                {
                    "key": "enerRech_",
                    "value": 8.3
                },
                {
                    "key": "def",
                    "value": 28
                }
            ]
        },
        {
            "setKey": "PrayersForDestiny",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 167
                },
                {
                    "key": "critRate_",
                    "value": 2.5
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "def",
                    "value": 17
                },
                {
                    "key": "critDMG_",
                    "value": 5.6
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "WanderersTroupe",
            "slotKey": "sands",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 3.3
                },
                {
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 5
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "hp_",
                    "value": 3.3
                },
                {
                    "key": "critRate_",
                    "value": 2.2
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "GladiatorsFinale",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 3.7
                },
                {
                    "key": "atk",
                    "value": 11
                },
                {
                    "key": "eleMas",
                    "value": 17
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "flower",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "hp",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.2
                },
                {
                    "key": "enerRech_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 17
                },
                {
                    "key": "hp_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critDMG_",
                    "value": 5
                },
                {
                    "key": "hp",
                    "value": 239
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "atk_",
                    "value": 4.7
                },
                {
                    "key": "critDMG_",
                    "value": 5
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def",
                    "value": 19
                },
                {
                    "key": "enerRech_",
                    "value": 4.7
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "sands",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 13
                },
                {
                    "key": "atk",
                    "value": 14
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "TheExile",
            "slotKey": "goblet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "hp_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 3.6
                },
                {
                    "key": "hp",
                    "value": 215
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Instructor",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 15
                },
                {
                    "key": "hp",
                    "value": 191
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Berserker",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "def_",
                    "value": 4.1
                },
                {
                    "key": "critDMG_",
                    "value": 4.4
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Berserker",
            "slotKey": "plume",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "atk",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "enerRech_",
                    "value": 5.2
                },
                {
                    "key": "critRate_",
                    "value": 2.8
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Berserker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "eleMas",
                    "value": 17
                },
                {
                    "key": "atk_",
                    "value": 3.7
                },
                {
                    "key": "def",
                    "value": 17
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        },
        {
            "setKey": "Berserker",
            "slotKey": "circlet",
            "level": 0,
            "rarity": 4,
            "mainStatKey": "def_",
            "location": "",
            "lock": false,
            "substats": [{
                    "key": "critRate_",
                    "value": 2.2
                },
                {
                    "key": "atk",
                    "value": 12
                },
                {
                    "key": "",
                    "value": 0
                },
                {
                    "key": "",
                    "value": 0
                }
            ]
        }
    ],
    "weapons": [{
            "key": "SkywardHarp",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Tartaglia"
        },
        {
            "key": "LostPrayerToTheSacredWinds",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Yanfei"
        },
        {
            "key": "StaffOfHoma",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "HuTao"
        },
        {
            "key": "SkywardAtlas",
            "level": 50,
            "ascension": 2,
            "refinement": 1,
            "location": "Barbara"
        },
        {
            "key": "Frostbearer",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Ningguang"
        },
        {
            "key": "DragonspineSpear",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "Deathmatch",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Zhongli"
        },
        {
            "key": "BlackcliffPole",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Xiao"
        },
        {
            "key": "Whiteblind",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "Noelle"
        },
        {
            "key": "PrototypeArchaic",
            "level": 90,
            "ascension": 6,
            "refinement": 5,
            "location": "Diluc"
        },
        {
            "key": "CinnabarSpindle",
            "level": 90,
            "ascension": 6,
            "refinement": 5,
            "location": "Albedo"
        },
        {
            "key": "FesteringDesire",
            "level": 90,
            "ascension": 6,
            "refinement": 5,
            "location": "Bennett"
        },
        {
            "key": "BlackcliffLongsword",
            "level": 90,
            "ascension": 6,
            "refinement": 1,
            "location": "KamisatoAyaka"
        },
        {
            "key": "TheStringless",
            "level": 80,
            "ascension": 5,
            "refinement": 3,
            "location": "Venti"
        },
        {
            "key": "WindblumeOde",
            "level": 70,
            "ascension": 4,
            "refinement": 5,
            "location": "Fischl"
        },
        {
            "key": "TheBell",
            "level": 70,
            "ascension": 4,
            "refinement": 3,
            "location": "Beidou"
        },
        {
            "key": "TheBlackSword",
            "level": 70,
            "ascension": 4,
            "refinement": 1,
            "location": "Keqing"
        },
        {
            "key": "PrototypeRancour",
            "level": 70,
            "ascension": 4,
            "refinement": 2,
            "location": ""
        },
        {
            "key": "SacrificialSword",
            "level": 70,
            "ascension": 4,
            "refinement": 3,
            "location": "Xingqiu"
        },
        {
            "key": "IronSting",
            "level": 60,
            "ascension": 3,
            "refinement": 1,
            "location": "Qiqi"
        },
        {
            "key": "FavoniusSword",
            "level": 60,
            "ascension": 3,
            "refinement": 2,
            "location": "Jean"
        },
        {
            "key": "PrototypeAmber",
            "level": 50,
            "ascension": 2,
            "refinement": 1,
            "location": "Sucrose"
        },
        {
            "key": "FavoniusCodex",
            "level": 50,
            "ascension": 2,
            "refinement": 5,
            "location": ""
        },
        {
            "key": "LuxuriousSeaLord",
            "level": 50,
            "ascension": 3,
            "refinement": 5,
            "location": "Chongyun"
        },
        {
            "key": "SacrificialGreatsword",
            "level": 50,
            "ascension": 2,
            "refinement": 2,
            "location": ""
        },
        {
            "key": "PrototypeStarglitter",
            "level": 40,
            "ascension": 1,
            "refinement": 1,
            "location": "Xiangling"
        },
        {
            "key": "FavoniusWarbow",
            "level": 21,
            "ascension": 1,
            "refinement": 1,
            "location": "Diona"
        },
        {
            "key": "Rust",
            "level": 6,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "MouunsMoon",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "SacrificialBow",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheStringless",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "FavoniusWarbow",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "DodocoTales",
            "level": 1,
            "ascension": 0,
            "refinement": 5,
            "location": ""
        },
        {
            "key": "MappaMare",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "SacrificialFragments",
            "level": 1,
            "ascension": 0,
            "refinement": 3,
            "location": ""
        },
        {
            "key": "SacrificialFragments",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheWidsith",
            "level": 1,
            "ascension": 0,
            "refinement": 3,
            "location": "Lisa"
        },
        {
            "key": "TheWidsith",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheWidsith",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheWidsith",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "FavoniusCodex",
            "level": 1,
            "ascension": 0,
            "refinement": 2,
            "location": ""
        },
        {
            "key": "FavoniusCodex",
            "level": 1,
            "ascension": 0,
            "refinement": 2,
            "location": ""
        },
        {
            "key": "DebateClub",
            "level": 20,
            "ascension": 1,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "FavoniusGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 3,
            "location": ""
        },
        {
            "key": "SnowTombedStarsilver",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "Rainslasher",
            "level": 1,
            "ascension": 0,
            "refinement": 4,
            "location": "Razor"
        },
        {
            "key": "Rainslasher",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "Rainslasher",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "Rainslasher",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "SacrificialGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "FavoniusGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 3,
            "location": ""
        },
        {
            "key": "FavoniusGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "LionsRoar",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheFlute",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "TheFlute",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Kaeya"
        },
        {
            "key": "EmeraldOrb",
            "level": 40,
            "ascension": 1,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "DebateClub",
            "level": 20,
            "ascension": 1,
            "refinement": 1,
            "location": ""
        },
        {
            "key": "Slingshot",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Amber"
        },
        {
            "key": "HuntersBow",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Aloy"
        },
        {
            "key": "HuntersBow",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "KujouSara"
        },
        {
            "key": "BeginnersProtector",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Thoma"
        },
        {
            "key": "BeginnersProtector",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Rosaria"
        },
        {
            "key": "WasterGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Sayu"
        },
        {
            "key": "WasterGreatsword",
            "level": 1,
            "ascension": 0,
            "refinement": 1,
            "location": "Xinyan"
        }
    ]
}