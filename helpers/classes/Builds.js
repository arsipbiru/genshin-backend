class Builds {
    constructor(models) {
        this.models = models
        this.sequelize = models.sequelize
    }

    getCharacterInfo = async (req, res) => {
        try {
            let query = req.query
            let chara_key = query?.chara_key
            query = `
                select bb.*, ci.*, cg.link chara_image, ca.*, a.asset_value element_image, a2.asset_value weapon_type_image,
                (min_atk * clm_value) + casm_value stat_min_atk,
                (min_def * clm_value) + casm_value stat_min_def,
                (min_hp * clm_value) + casm_value stat_min_hp,
                (max_atk * clm_value) + casm_value stat_max_atk,
                (max_def * clm_value) + casm_value stat_max_def,
                (max_hp * clm_value) + casm_value stat_max_hp,
                attribute_value * casbm_multiplier bonus_attribute_value,
                bonus_attribute
                from (
                    select item ->> 'key' chara_key, item ->> 'level' lvl, item ->> 'constellation' constellation, item ->> 'ascension' ascension, item ->> 'talent' talent from (
                        select json_array_elements(user_storage -> 'characters') item from masterdata.users u 
                    ) aa
                    where item ->> 'key' = '${chara_key}'
                ) bb
                join masterdata.character_info ci on ci.chara_key = bb.chara_key
                join masterdata.character_ascension ca on ca.chara_phase = bb.ascension
                join masterdata.character_gallery cg on cg.character_key = bb.chara_key and cg.image = 'CharacterCard'
                join masterdata.assets a on a.asset_key = ci.chara_element 
                join masterdata.assets a2 on a2.asset_key = ci.chara_wp_key  
                join masterdata.character_stat cs on cs.character_key = bb.chara_key
                join masterdata.character_ascension_stat_multiplier casm on casm.casm_phase = bb.ascension
                join masterdata.character_level_multiplier clm on clm.clm_level = bb.lvl and clm.clm_star = ci.chara_rarity
                join masterdata.character_ascension_stat_bonus_multiplier casbm on casbm.casbm_phase = bb.ascension
            `.trim()
            let data = await this.sequelize
                .query(query, { raw: true })
            data = data?.[0]?.[0]
            res.send({data})
        } catch (err) {
            res.send(err)
        }
    }

    getCharacterWeapon = async (req, res) => {
        try {
            let query = req.query
            let chara_key = query?.chara_key
            query = `
                select * from (
                    select item ->> 'key' weap_key, item ->> 'level' weap_level, item ->> 'refinement' weap_refinement, item ->> 'ascension' weap_asc, item ->> 'location' weap_loc
                    from (
                        select json_array_elements(user_storage -> 'weapons') as item from masterdata.users u
                    ) dd
                    where item ->> 'location' = '${chara_key}'
                ) aa
                join masterdata.weapon_info wi on wi.wp_key = weap_key
                join masterdata.weapon_ascension wa on weap_asc = wa.wpa_phase 
                join masterdata.weapon_main_stat_progression wmsp on wmsp.wpmsp_rarity = wi.wp_rarity and wmsp.wpmsp_base = wi.wp_mainstat_base and wmsp.wpmsp_level = weap_level
                join masterdata.weapon_substat_progression wsp on wsp.wpsp_level = weap_level and wsp.wpsp_id = wi.wp_wpsp_id
                join masterdata.stats s on s.stat_key = wi.wp_substat_stat_key 
                join masterdata.weapon_refinement wr on wr.wpr_wp_key = weap_key and wr.wpr_rank = weap_refinement
            `.trim()
            let data = await this.sequelize
                .query(query, { raw: true })
            data = data[0][0]
            res.send({data})
        } catch (err) {
            res.send(err)
        }
    }
}

module.exports = Builds 