class Inventory {
    constructor(models) {
        this.models = models
        this.sequelize = models.sequelize
    }

    getCharacters = async (req, res) => {
        try {
            let query = req.query
            let perPage = query?.perPage || 10
            let page = query?.page || 1
            let sortBy = query?.sortBy || 'lvl'
            let sortOrder = query?.sortOrder || 'asc'
            let order = `order by ${sortBy} ${sortOrder}`

            let where = 'where 1=1 '
            let where_rarity = query?.rarity
            let where_weapon = query?.weapon
            let where_element = query?.element 
            let where_not_chara = query?.isNotChara
            if(where_rarity != null) { 
                where += `chara_rarity in (${where_rarity.split(',').map(item => (`'${item}'`))}) `
            }
            if(where_weapon != null) {
                if(where != `where `) where += 'and '
                where += `chara_wp_key in (${where_weapon.split(',').map(item => (`'${item}'`))}) `
            } 
            if(where_element != null) {
                if(where != `where `) where += 'and '
                where += `chara_element in (${where_element.split(',').map(item => (`'${item}'`))}) `
            } 
            if(where_not_chara != null){
                if(where != `where `) where += 'and '
                where += `chara_key not in (${where_not_chara.split(',').map(item => (`'${item}'`))}) `
            }
            if(where == `where `) where = ''

            query = `
            select  *,
            (min_atk * clm_value) + casm_value stat_min_atk,
            (min_def * clm_value) + casm_value stat_min_def,
            (min_hp * clm_value) + casm_value stat_min_hp,
            (max_atk * clm_value) + casm_value stat_max_atk,
            (max_def * clm_value) + casm_value stat_max_def,
            (max_hp * clm_value) + casm_value stat_max_hp
            from (
                select
                lvl, chara_cons, chara_ascension, cast(chara_talent as json) chara_talent,
                ci.chara_name, ci.chara_rarity as rarity, ci.chara_element, ci.chara_wp_key,
                a.asset_value as element_icon,
                a.asset_light_image as element_light,
                a.asset_element_color_coded as element_color,
                ci.chara_key,
                cs.*,
                casm.* ,
                clm.*,
                a2.asset_value as weapon_type_icon,
                faceicon.link as faceicon_image,
                bgicon.link as bgicon_image,
                sideicon.link as sideicon_image
                from (
                    select item ->> 'key' as chara_key, item ->> 'level' lvl, item ->> 'constellation' chara_cons,
                    item ->> 'ascension' chara_ascension, item ->> 'talent' chara_talent
                    from (
                        select json_array_elements(user_storage -> 'characters') as item from masterdata.users u
                    ) dd
                ) cc
                join masterdata.character_info ci on ci.chara_key = cc.chara_key
                join masterdata.assets a on a.asset_key = ci.chara_element
                join masterdata.character_stat cs on cs.character_key = cc.chara_key
                join masterdata.character_ascension_stat_multiplier casm on casm.casm_phase = cc.chara_ascension
                join masterdata.character_level_multiplier clm on clm.clm_level = cc.lvl and clm.clm_star = ci.chara_rarity
                join masterdata.assets a2 on a2.asset_key = ci.chara_wp_key
                join masterdata.character_gallery faceicon on faceicon.image = 'FaceIcon' and faceicon.character_key = ci.chara_key
                join masterdata.character_gallery sideicon on sideicon.image = 'SideIcon' and sideicon.character_key = ci.chara_key
                join masterdata.character_gallery bgicon on bgicon.image = 'PartyBackground' and bgicon.character_key = ci.chara_key
            ) bb
            ${where} ${order}
            limit ${perPage} offset ${(page - 1) * perPage}
            `.trim() 
            console.log(query)
            let data = await this.sequelize
                .query(query, { raw: true })
            data = data[0]    

            res.send({data})
        } catch (err) {
            res.send(err)
        }
    }

    getArtifacts = async (req, res) => {
        try {
            let query = req.query
            let perPage = query?.perPage || 10
            let page = query?.page || 1
            let sortBy = query?.sortBy || 'lvl'
            let sortOrder = query?.sortOrder || 'asc'
            let order = `order by ${sortBy} ${sortOrder}`

            let where = 'where '
            let where_rarity = query?.rarity
            let where_sets = query?.sets
            let where_slot = query?.slot
            let where_stats = query?.stats
            let where_location = query?.location
            if(where_rarity != null && where_rarity != '') { 
                where += `arte ->> 'rarity' in (${where_rarity.split(',').map(item => (`'${item}'`))}) `
            }
            if(where_sets != null && where_sets != '') {
                if(where != `where `) where += 'and '
                where += `arte ->> 'setKey' in (${where_sets.split(',').map(item => (`'${item}'`))}) `
            }
            if(where_slot != null && where_slot != '') {
                if(where != `where `) where += 'and '
                where += `arte ->> 'slotKey' in (${where_slot.split(',').map(item => (`'${item.toLowerCase()}'`))}) `
            }
            if(where_location != null && where_location != '') {
                if(where != `where `) where += 'and '
                where += `arte ->> 'location' = '${where_location}'`
            }
            if(where_stats != null && where_stats != '') {
                if(where != `where `) where += 'and '
                where += `arte ->> 'mainStatKey' in (${where_stats.split(',').map(item => (`'${item}'`))}) `
            }
            if(where == `where `) where = ''  
            let filter_only_default = (!where_stats || where_stats == '') || sortBy != 'current_efficiency' 
            query = `
                select
                    cast(lvl as int), cast(rarity as int), sub_stat, chara_equipped, cast(arte_locked as boolean), arset_name, ar_icon, arset_piece2, 
                    arset_piece4, arset_piece1, ar_name, slots, arms_value as main_stat, stat_name as main_stat_label, main_stat_key, stat_key, set_key,
                    current_efficiency, 37 as max_efficiency
                from ( 
                    select 
                        arte ->> 'setKey' set_key, 
                        arte ->> 'slotKey' slots, 
                        cast(arte ->> 'level' as int) lvl, 
                        arte ->> 'rarity' rarity, 
                        arte ->> 'mainStatKey' main_stat_key,
                        cast(arte ->> 'substats' AS json) sub_stat, 
                        arte ->> 'location' chara_equipped, 
                        arte ->> 'lock' arte_locked 
                    from ( 
                        select json_array_elements(user_storage -> 'artifacts') arte 
                        from masterdata.users u  
                    ) ar  
                    ${filter_only_default ? `${where} ${order} limit ${perPage} offset ${(page - 1) * perPage}`  : ''}
                ) artifacts 
                left join masterdata.artifact_set arte_set on arte_set.arset_key = artifacts.set_key 
                left join masterdata.artifact_info ai on lower(ai.ar_slot) = artifacts.slots and ai.ar_arset_key = artifacts.set_key 
                left join masterdata.artifact_main_stat main_stat on main_stat.arms_rarity = artifacts.rarity and main_stat.arms_level = artifacts.lvl and main_stat.arms_stat_key = artifacts.main_stat_key
                left join masterdata.stats stat on stat.stat_key = artifacts.main_stat_key 
                left join (
                    select sum(cast(arsst_tier as integer)) current_efficiency, sub_stat_master from 
                    (
                        select * from (
                            select cast(sub_stat_master as varchar), sub_stat ->> 'key' skey, cast(sub_stat ->> 'value' as float) as value, rarity from 
                            (
                                select 
                                    json_array_elements(sub_stat_master) sub_stat,
                                    sub_stat_master, rarity
                                from 
                                (
                                    select 
                                    arte ->> 'setKey' set_key, 
                                    arte ->> 'slotKey' slots , 
                                    arte ->> 'level' lvl, arte ->> 'rarity' rarity, 
                                    arte ->> 'mainStatKey' main_stat, 
                                    cast(arte ->> 'substats' as json) sub_stat_master, 
                                    arte ->> 'location' chara_equipped, 
                                    arte ->> 'lock' arte_locked 
                                    from ( 
                                        select json_array_elements(user_storage -> 'artifacts') arte 
                                        from masterdata.users u 
                                        where u.user_id = '1' 
                                    ) ar  
                                ) satu_row
                            ) artes
                        ) arted
                        left join masterdata.artifact_sub_stat_tiers asst on cast(asst.arsst_score as float) = arted.value and asst.arsst_stat_key = arted.skey and asst.arsst_rarity = arted.rarity
                    ) af group by sub_stat_master
                ) sub_arte on sub_arte.sub_stat_master = cast(artifacts.sub_stat as varchar) 
                ${!filter_only_default ? `${where} ${order} limit ${perPage} offset ${(page - 1) * perPage}`  : ''}  
            `.trim() 
            let data = await this.sequelize
                .query(query, { raw: true })
            data = data[0]    

            res.send({data})
        } catch (err) {
            res.send(err)
        }
    } 

    getSubStatTier = async (req, res) => {
        try {
            let data = await this.sequelize
                .query(`select *, cast(arsst_score as float) score from masterdata.artifact_sub_stat_tiers`.trim(), { raw: true })
            data = data[0]
            res.send({data})
        } catch (err) {
            res.send(err)
        }
    } 

    getWeapons = async (req, res) => {
        try {
            let query = req.query
            let perPage = query?.perPage || 10
            let page = query?.page || 1
            let sortBy = query?.sortBy || 'lvl'
            let sortOrder = query?.sortOrder || 'asc'
            let order = `order by ${sortBy} ${sortOrder}`

            let where = 'where '
            let where_rarity = query?.rarity
            let where_type = query?.type 
            let where_stat = query?.stats 
            if(where_rarity != null) { 
                where += `wp_rarity in (${where_rarity.split(',').map(item => (`'${item}'`))}) `
            }
            if(where_type != null) {
                if(where != `where `) where += 'and '
                where += `wp_type in (${where_type.split(',').map(item => (`'${item}'`))}) `
            } 
            if(where_stat != null) {
                if(where != `where `) where += 'and '
                where += `stat_key in (${where_stat.split(',').map(item => (`'${item}'`))}) `
            } 
            if(where == `where `) where = ''
            let data = await this.sequelize
                .query(`
                select weaps.*, wi.wp_type, wi.wp_rarity rarity, wi.wp_icon, wi.wp_name, wr.wpr_name refinement_title, wr.wpr_description refinement_desc, 
                    wmsp.wpmsp_progression mainstat_value, s.stat_name substat_label, s.stat_key, wsp.wpsp_value substat_value, wa.wpa_max_level max_level,
                    weap_ascension
                from (
                    select 
                        weap ->> 'level' lvl, 
                        weap ->> 'ascension' weap_ascension, 
                        weap ->> 'refinement' weap_refinement, 
                        weap ->> 'location' chara_equipped,
                        weap ->> 'key' weap_key
                    from (
                        select 
                            json_array_elements(user_storage -> 'weapons') weap 
                        from 
                            masterdata.users u 
                        where 
                            u.user_id = '1' 
                    ) wpl 
                ) weaps 
                join masterdata.weapon_info wi on weap_key = wi.wp_key 
                join masterdata.weapon_refinement wr on weap_refinement = wr.wpr_rank 
                and weap_key = wr.wpr_wp_key 
                join masterdata.weapon_main_stat_progression wmsp on lvl = wmsp.wpmsp_level 
                and wi.wp_rarity = wmsp.wpmsp_rarity 
                and wi.wp_mainstat_base = wmsp.wpmsp_base 
                left join masterdata.stats s on wi.wp_substat_stat_key = s.stat_key 
                left join masterdata.weapon_substat_progression wsp on wi.wp_wpsp_id = wsp.wpsp_id and lvl = wsp.wpsp_level
                left join masterdata.weapon_ascension wa on wa.wpa_phase = weaps.weap_ascension
                ${where}
                ${order}
                limit ${perPage} offset ${(page - 1) * perPage}
                `.trim(), { raw: true })
            data = data[0]    

            res.send({data})
        } catch (err) {
            res.send(err)
        }
    }
}

module.exports = Inventory