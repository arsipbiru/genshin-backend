
module.exports = (sequelize, Sequelize) => {
    const Model = sequelize.define("artifact_set", {
        arset_key: {
            type: Sequelize.STRING,
            primaryKey: true, 
        },
        arset_name : {
            type: Sequelize.STRING, 
        }, 
        arset_piece2 : {
            type: Sequelize.STRING, 
        }, 
        arset_piece4 : {
            type: Sequelize.STRING, 
        }, 
        arset_piece1 : {
            type: Sequelize.STRING, 
        }, 
    }, {timestamps: false, schema: 'masterdata',freezeTableName: true,});
    Model.associate = function(models) {
        // Model.belongsTo(models.equipment_stats, { foreignKey: 'artilvl_stattype', targetKey: 'eqstat_id' });
    };
    return Model;
};