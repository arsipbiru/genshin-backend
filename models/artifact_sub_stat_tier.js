
module.exports = (sequelize, Sequelize) => {
    const Model = sequelize.define("artifact_sub_stat_tier", {
        arsst_id: {
            type: Sequelize.BIGINT,
            primaryKey: true, 
            autoIncrement: true
        },
        arsst_stat_key : {
            type: Sequelize.STRING, 
        }, 
        arsst_rarity : {
            type: Sequelize.STRING, 
        }, 
        arsst_tier : {
            type: Sequelize.STRING, 
        }, 
        arsst_score : {
            type: Sequelize.STRING, 
        }, 
    }, {timestamps: false, schema: 'masterdata',});
    Model.associate = function(models) {
        // Model.belongsTo(models.equipment_stats, { foreignKey: 'artilvl_stattype', targetKey: 'eqstat_id' });
    };
    return Model;
};