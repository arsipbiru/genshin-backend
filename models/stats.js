
module.exports = (sequelize, Sequelize) => {
    const Model = sequelize.define("stats", {
        stat_key: {
            type: Sequelize.STRING,
            primaryKey: true, 
        },
        stat_name : {
            type: Sequelize.STRING, 
        }, 
    }, {timestamps: false, schema: 'masterdata',});
    Model.associate = function(models) {
        // Model.belongsTo(models.equipment_stats, { foreignKey: 'artilvl_stattype', targetKey: 'eqstat_id' });
    };
    return Model;
};