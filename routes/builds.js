const router = require('express').Router()
const db = require("../models");

const BuildsClass = require("../helpers/classes/Builds");
const controller = new BuildsClass(db);

const checkBodyIsEmpty = require('../helpers/checkBodyIsEmpty') 

router.get("/character-info", controller.getCharacterInfo);
router.get("/character-weapon", controller.getCharacterWeapon);

module.exports = router