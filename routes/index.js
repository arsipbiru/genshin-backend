
const router = require('express').Router()

router.use('/masterdata', require('./masterdata/index'))
router.use('/inventory', require('./inventory'))
router.use('/builds', require('./builds'))

module.exports = router