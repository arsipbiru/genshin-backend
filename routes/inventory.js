const router = require('express').Router()
const db = require("../models");

const InventoryClass = require("../helpers/classes/Inventory");
const controller = new InventoryClass(db);

const checkBodyIsEmpty = require('../helpers/checkBodyIsEmpty') 

router.get("/artifacts/sub_stat_tiers", controller.getSubStatTier);
router.get("/artifacts", controller.getArtifacts);
router.get('/characters', controller.getCharacters);
router.get("/weapons", controller.getWeapons);

module.exports = router