
const router = require('express').Router() 

router.use('/stats', require('./stats')) 
router.use('/artifact_sub_stat_tier', require('./artifact_sub_stat_tier')) 
router.use('/artifact_set', require('./artifact_set')) 

module.exports = router