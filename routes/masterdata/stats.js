const router = require('express').Router()
const db = require("../../models");

const MainModel = db.stats;

const BasicCrud = require("../../helpers/classes/BasicCrud");
const controller = new BasicCrud(MainModel);

const checkBodyIsEmpty = require('../../helpers/checkBodyIsEmpty') 

router.get("/", controller.get);
router.post("/", checkBodyIsEmpty, controller.save);
router.delete("/:stat_key", controller.delete);

module.exports = router